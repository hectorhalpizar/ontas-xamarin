﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ontas.models
{
    public class User
    {
        public User(string username, string password, string email, string phoneNumber)
        {
            Username = username;
            Password = password;
            Email = email;
            PhoneNumber = phoneNumber;
        }

        public string Id { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public DateTime RegistrationDate { get; set; }
    }
}
