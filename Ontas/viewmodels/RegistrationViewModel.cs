﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Xamarin.Forms;
using Ontas.models;

namespace Ontas.viewmodels
{
    public class RegistrationViewModel : INotifyPropertyChanged
    {
        public RegistrationViewModel()
        {
            IsPasswordValid = true;
            IsEmailFormatValid = true;

            SignUpCommand = new Command(() =>
            {
                ErrorMessage = "";

                IsPasswordValid = Password == PasswordConfirm;
                IsEmailFormatValid = ValidateEmail();

                // TODO - validate existing usernames
                // TODO - validate existing email
                if (!IsPasswordValid)
                {
                    ErrorMessage = "Password Fields doesn´t match.";
                }
                else if (!IsEmailFormatValid)
                {
                    ErrorMessage = "E-Mail has an invalid format.";
                }
                else
                {                    
                    var user = new User(Username, Password, Email, MobilePhone);

                    // TODO - Do request for registation an validate responses.
                }
            });
        }

        #region User Properties
        private string username, password, passwordConfirm, email, mobilePhone, errorMessage;

        public string Username
        {
            get => username;

            set
            {
                username = value;

                var args = new PropertyChangedEventArgs(nameof(Username));
                PropertyChanged?.Invoke(this, args);
            }
        }

        public string Password
        {
            get => password;

            set
            {
                password = value;

                var args = new PropertyChangedEventArgs(nameof(Password));
                PropertyChanged?.Invoke(this, args);
            }
        }

        public string PasswordConfirm
        {
            get => passwordConfirm;

            set
            {
                passwordConfirm = value;

                var args = new PropertyChangedEventArgs(nameof(PasswordConfirm));
                PropertyChanged?.Invoke(this, args);
            }
        }

        public string Email
        {
            get => email;

            set
            {
                email = value;

                var args = new PropertyChangedEventArgs(nameof(Email));
                PropertyChanged?.Invoke(this, args);
            }
        }

        public string MobilePhone
        {
            get => mobilePhone;

            set
            {
                mobilePhone = value;

                var args = new PropertyChangedEventArgs(nameof(MobilePhone));
                PropertyChanged?.Invoke(this, args);
            }
        }

        public string ErrorMessage
        {
            get => errorMessage;

            set
            {
                errorMessage = value;

                var args = new PropertyChangedEventArgs(nameof(ErrorMessage));
                PropertyChanged?.Invoke(this, args);
            }
        }
        #endregion

        #region Field Properties
        bool isPasswordValid, isEmailFormatValid;

        public bool IsEmailFormatValid
        {
            get => isEmailFormatValid;

            set
            {
                isEmailFormatValid = value;

                var args = new PropertyChangedEventArgs(nameof(IsEmailFormatValid));
                PropertyChanged?.Invoke(this, args);
            }
        }

        public bool IsPasswordValid
        {
            get => isPasswordValid;

            set
            {
                isPasswordValid = value;

                var args = new PropertyChangedEventArgs(nameof(IsPasswordValid));
                PropertyChanged?.Invoke(this, args);
            }
        }
        #endregion

        public Command SignUpCommand { get; }

        #region Methods
        public bool ValidateEmail()
        {
            // TODO: Do a regex validation
            return !String.IsNullOrWhiteSpace(Email) && Email.Contains("@");
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
