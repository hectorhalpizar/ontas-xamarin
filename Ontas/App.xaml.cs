﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Ontas.views.users;

namespace Ontas
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Registration());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
